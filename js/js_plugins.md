## vue 开发 plugin

1. vue一系列的扩展插件(库):
    * vue-cli: vue脚手架
    * vue-resource(axios): ajax请求
    * vue-router: 路由
    * vuex: 状态管理
    * vue-lazyload: 图片懒加载
    * vue-scroller: 页面滑动相关
    * mint-ui: 基于vue的组件库(移动端)
    * element-ui: 基于vue的组件库(PC端)

2. vue 开发插件
    * Vue.js devtools:vue chrome 开发插件, [下载插件](http://chromecj.com/web-development/2018-01/886.html) [查看源码](https://github.com/vuejs/vue-devtools)

## 常用 JS 第三方框架

1. [My97DatePicker](http://www.my97.net/demo/index.htm): 获取日期时间的js控件
1. [jqgrid](https://www.cnblogs.com/jiangxifanzhouyudu/p/7411308.html) : 显示网格数据的jQuery插件，通过使用jqGrid可以轻松实现前端页面与后台数据的ajax异步通信。
1. [lay](https://layer.layui.com/) : web弹层组件
1. [layui](https://www.layui.com/doc/modules/layer.html) : 采用自身模块规范编写的前端 UI 框架
1. [jQuery TreeGrid](http://maxazan.github.io/jquery-treegrid/)
1. [ztree](https://github.com/zTree/zTree_v3)
