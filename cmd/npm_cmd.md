# npm 常用命令
## 使用原生资源命令
```
# 安装依赖，使用淘宝资源命令 cnpm
npm install

# 启动应用，地址为 localhost:8080
npm run dev

# 如果你需要发布到正式环境可以执行以下命令：
npm run build
```

## 使用淘宝资源命令
```
# 安装依赖，使用淘宝资源命令 cnpm
cnpm install

# 启动应用，地址为 localhost:8080
cnpm run dev

# 如果你需要发布到正式环境可以执行以下命令：
cnpm run build
```
